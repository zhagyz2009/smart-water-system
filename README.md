# 智慧水务系统

#### 介绍
面向一个区域的智慧水务管理系统，提供源码学习，禁止商用，商用带来的一切法律后果自负，与本人无关！联系微信：ubao188。

#### 软件架构
Java语言开发，前后端分离。

#### 界面展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/131348_f69dc550_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/131508_92702b47_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/131531_f1b01e23_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/131652_588531f9_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/131722_9a377426_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132326_ae4b51ae_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132414_10675ffb_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132437_6e88ee87_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132511_b8485cc1_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132612_1c599243_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132651_28ed78f5_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132754_6e58d654_8867896.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0325/132815_3483a2a8_8867896.png "屏幕截图.png")